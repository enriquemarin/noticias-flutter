import 'dart:async';
import 'dart:convert';
import 'package:noticias/model/noticia.dart';
import 'package:http/http.dart' as http;

class Requests {
  static Future<List<Noticia>> peticionNoticias(recurso) async{
    var url = 'https://newsapi.org/v2/top-headlines?country=mx&language=es&apiKey=4c8d1fc3497c4134894831844f661333';
    if(recurso !=null){
      url = 'https://newsapi.org/v2/top-headlines?sources=$recurso&language=es&apiKey=4c8d1fc3497c4134894831844f661333';
    }
    var respuesta = await http.get(url);
    if(respuesta.statusCode == 200){
        var jsonRespuesta = json.decode(respuesta.body);

        //print('Respuesta JSON');
        //print(jsonRespuesta);

        var listaNoticias = Noticia.fromJson(jsonRespuesta['articles']);
        return listaNoticias;
    }
    List<Noticia> no = [];
    return no;
  }

  static Future<List<dynamic>> peticionRecursos() async{
    var respuesta = await http.get('https://newsapi.org/v2/sources?language=es&apiKey=4c8d1fc3497c4134894831844f661333');
    if(respuesta.statusCode == 200){
        var jsonRespuesta = json.decode(respuesta.body);
        print(jsonRespuesta['sources'].length);
        List<dynamic> listaRecursos = [];
        for (var i = 0; i < jsonRespuesta['sources'].length ; i++)
        {
          listaRecursos.add(
            {
              'id': jsonRespuesta['sources'][i]['id'],
              'name': jsonRespuesta['sources'][i]['name']
            }
          );
        } 
        return listaRecursos;
    }
    List<dynamic> no = [];
    return no;
  }
}