import 'package:flutter/material.dart';
import 'NewsCard.dart';
import 'package:noticias/model/noticia.dart';
import 'dart:async';
import 'package:noticias/controller/requests.dart';
import 'package:async_loader/async_loader.dart';

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _homeState();
}

class _homeState extends State<Home> {
  var _recursos = [];
  var _recursoSeleccionado;
  final GlobalKey<AsyncLoaderState> _asyncloaderState = 
    new GlobalKey<AsyncLoaderState>();

  getMessage() async{
    return new Future.delayed(Duration(seconds: 5), () => 'Cargado correctamente');
  }
  
  obteniendoDatosImportantes() async{
    var recursos = await Requests.peticionRecursos();
    recursos.add({
      'id': 'todos',
      'name': 'Ver Todas'
    });
    setState(() {
      _recursos = recursos;
    });
    return await Requests.peticionNoticias(_recursoSeleccionado);
  }

  @override
  Widget build(BuildContext context) {

    var _asyncLoader = new AsyncLoader(
      key: _asyncloaderState,
      initState: () async => await obteniendoDatosImportantes(),
      renderLoad: () => Center(child: CircularProgressIndicator()),
      renderError: ([error]) => Center(child: Text('Ocurrió un error'),),
      renderSuccess: ({data}) => Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView.builder(
          itemBuilder: (BuildContext context, index){
            return NewsCard(data[index]);
          },
          itemCount: data.length,
        ),
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        title: Text("Noticias"),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (seleccion){
              //print(seleccion);
              if(seleccion == 'todos'){
                setState(() {
                  _recursoSeleccionado = null;
                });
              }else{
                setState(() {
                  _recursoSeleccionado = seleccion;
                });
              }
              _asyncloaderState.currentState.reloadState();
            },
            itemBuilder: (BuildContext context){
              return _recursos.map((objeto){
                return PopupMenuItem(
                  value: objeto['id'],
                  child: Text(objeto['name'])
                );
              }).toList();
            },
          )
        ],
      ),
      body: 
        _asyncLoader
   /*    new NewsCard(new Noticia(
        titulo: 'Mi noticia',
        descripcion: 'descripción de la noticia',
        imagenUrl: 'https://www.nationalgeographic.com.es/medio/2018/02/27/playa-de-isuntza-lekeitio__1280x720.jpg',
      )), */
    );
  }
}