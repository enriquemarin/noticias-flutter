import 'package:flutter/material.dart';
import 'package:noticias/model/noticia.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';

class NewsCard extends StatefulWidget {
  final Noticia noticia;
  NewsCard(this.noticia);
  @override
  State<StatefulWidget> createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      elevation: 5.0,
      margin: EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 20),
      shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
      child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Stack(
          alignment: Alignment(0.0, 1.0),
          children: <Widget>[
            ClipRRect(
                borderRadius: new BorderRadius.circular(15.0),
                child: Image.network(widget.noticia.imagenUrl)
            ),
            //Image.network(widget.noticia.imagenUrl),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Color.fromRGBO(0, 0, 0, 0.5),
                shape: BoxShape.rectangle,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  children: <Widget>[
                    new Text(
                      widget.noticia.titulo,
                      style: new TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: Theme.of(context).textTheme.title.fontSize,
                      ),
                    ),
                    new Text(
                      widget.noticia.descripcion.length >= 100 ? widget.noticia.descripcion.substring(0,100) : widget.noticia.descripcion ,
                      style: new TextStyle(
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        //Divider(),
        Container(
          child: Row(
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  print('url de la noticia: ${widget.noticia.urlNoticia}');
                  _openUrl(widget.noticia.urlNoticia);
                },
                child: Text("Ver Noticia"),
              ),
              FlatButton(
                onPressed: (){
                  Share.share('Te recomiendo ver esta noticia; ${widget.noticia.urlNoticia}');
                },
                child: Text('Compartir'),
              )
            ],
          ),
        ),
      ],
    ));
  }
  _openUrl(url) async{
    if(await canLaunch(url)){
      await launch(url, forceWebView: true);
    }else{
      throw 'Error no se puede abrir';
    }
  }
}
